import scrapy
import helper
import csv


class JobSpider(scrapy.Spider):

    name = "indeed_jobs"
    response = ""
    div_id_tag = ""
    raw_input = ""
    curr_category = ""
    base_url = "http://indeed.com"
    jobs_list = []
    link_list = []
    keyword_list = []
    ind = 0
    page_num = 0
    start_num = 0
    cat_iterator = 0
    max_jobs = 10
    links = {}
    cat_dict = {}

    def start_requests(self):
        self.get_list()
        urls = self.links

        for key in urls:
            yield scrapy.Request(url=urls[key], callback=self.parse)



    def parse(self, response):
        self.response = response
        #keyword = response.url.split("=")[1].split("&")[0].replace("+", " ")            #different from keyword_list
        keyword = self.keyword_list[self.cat_iterator]
        div_ids = response.css('div.row.result::attr(id)').extract()
        curr_job = {'keyword': '',
                    'category': '',
                    'position': '',
                    'company': '',
                    'reviews': '',
                    'location': '',
                    'summary': '',
                    'experience': '',
                    'link': '',
                    'company page': '',
                    'salary': ''}

        with open('output\ ' + keyword + '.csv', 'wb') as csvfile:
            filewriter = csv.writer(csvfile)
            filewriter.writerow(curr_job.keys())                                                   #column header in csv

            for div_id in div_ids:
                self.div_id_tag = 'div#' + div_id + ' '
                position = self. get_elem('h2 a.turnstileLink::attr(title)')
                link = self.base_url + self.get_elem('h2 a.turnstileLink::attr(href)')
                company = helper.get_inside(self.get_elem('span.company')).replace("\n", "").lstrip()
                reviews = self.get_elem('span.slNoUnderline::text')
                location = self.get_elem('span.location::text')
                summary = helper.get_inside(self.get_elem('span.summary')).replace("\n", "").replace("read more", "").lstrip()
                experience = self.get_elem('span.experienceList::text')
                company_page = self.base_url + self.get_elem('span ::attr(href)')
                salary = self.get_elem('span.no-wrap::text')
                category = self.cat_dict[keyword]

                filewriter.writerow([salary, company, company_page, link, category, keyword, experience, summary,
                                     reviews, location, position])
                curr_job = {'keyword': keyword,
                            'category': category,
                            'position': position,
                            'company': company,
                            'reviews': reviews,
                            'location': location,
                            'summary': summary,
                            'experience': experience,
                            'link': link,
                            'company page': company_page,
                            'salary': salary}

                self.ind += 1
                self.jobs_list.append(dict(curr_job))
            self.cat_iterator += 1
            self.page_num += 1
            self.start_num += 10
            jobs_scraped = self.ind

        if self.page_num == 1:
            next_url = response.url + '&start=' + str(self.start_num)
        else:
            next_url = response.url.rsplit('&start=')[0] + '&start=' + str(self.start_num)

        if self.max_jobs > jobs_scraped:
            yield scrapy.Request(url=next_url, callback=self.parse)
        else:
            self.ind = 0
            self.page_num = 0
            print self.jobs_list

        self.log('Saved file %s' % keyword + '.csv')
        print 'Saved file %s' % keyword + '.csv'

    def get_elem(self, elem):
        resp = self.response.css(self.div_id_tag + elem).extract_first()
        if resp is not None:
            return resp.strip().encode('ascii', 'ignore').decode()
        return resp


    def get_list(self):
        i = 0
        with open('input\input.csv', 'rb') as csvfile:
            next(csvfile)                                                                   # disregard header row
            filereader = csv.reader(csvfile)
            for row in filereader:
                for item in row:
                    item = item + '\n'
                    self.raw_input += item
        my_list = self.raw_input.splitlines()
        for item in my_list:
            if (i == 0 or i % 3 == 0):
                category = item
            elif (i == 1 or i % 3 == 1):
                input_keyword = item
                self.keyword_list.append(item)
            else:
                if(item != ''):
                    self.link_list.append(item)
                    self.links[input_keyword] = item
                    self.cat_dict[input_keyword] = category
                elif (item == ''):
                    self.keyword_list.pop()
            i += 1
