
#function to get inside tag
def get_inside(tag):
    while 1:
        start_ind = tag.find("<")
        if start_ind == -1:
            return tag
        end_index = tag.find(">") + 1
        repl = tag[start_ind:end_index]
        tag = tag.replace(repl, "")


def next_indeed_url(url, inc):
    start_str = "start"
    first_part = url
    next_start = inc
    if start_str in url:
        ind = url.find(start_str) - 1
        first_part = url[:ind]
        next_start = int(url[ind + 7:]) + inc
    return first_part + "&start=" + str(next_start)


def next_ziprecruiter_url(url, inc):
    start_str = "page"
    first_part = url
    next_start = inc
    if start_str in url:
        ind = url.find(start_str) - 1
        first_part = url[:ind]
        next_start = int(url[ind + 6:]) + inc
    return first_part + "&page=" + str(next_start)
